
<?php
error_reporting(0);
if($_POST['product2']==''){    
	$_POST['product2']=0;
}
if($_POST['product3']==''){
	$_POST['product3']=0;
}
?>
<!DOCTYPE html>
<html>
<head>
	<title></title>
	<link rel="stylesheet" type="text/css" href="files/css/bootstrap.min.css">
	<link rel="stylesheet" href="files/css/style.css">
	<script type="text/javascript" src="files/js/jquery.min.js"></script>
	
	
</head>
<body backgroundcolor="">
	<div class="container">
	<div class="card">
		<center><legend class="title">My shop's Invoice</legend></center>
	<table class="tab">
  <thead>
    <tr>
      <th >Buyer Name </th>
      <td ><?= $_POST['name_val']; ?></td>    <!-- Access Of Values from HTML Page  -->
        </tr>
    <tr>
      <th >Buyer Email </th>
      <td ><?= $_POST['email_val']; ?></td>
    </tr>
    <tr>
      <th >Buyer Phone </th>
      <td ><?php echo $_POST['phone_val']; ?></td>
    </tr>
    <tr>
      <th  style="text-align: top;">Address Is</th>
      <td >
      	<?php echo $_POST['address_val']; echo "<br>";
      
      	echo $_POST['city_val'];echo "<br>";
      	echo $_POST['postal_val'];
       ?></td>
    </tr>
    <tr> 
      <th ><?= $_POST['product1']; ?> Product1 @ $10.00</th>    
      <td >$<?php echo ($_POST['product1']*10); ?></td>
    </tr>
    <tr>
      <th ><?= $_POST['product2']; ?> Product2 @ $20.00</th>
      <td >$<?php echo ($_POST['product2']*20); ?></td>
    </tr>
    <tr>
      <th ><?= $_POST['product3']; ?> Product3 @ $30.00</th>
      <td >$<?php echo ($_POST['product3']*30); ?></td>
    </tr>
    <tr>
      <th >Shipping Charge</th>
      <td >
      	<?php
      	if($_POST['days_val']=="1"){ $s2=30; echo "$".$s2; } //day1
      	if($_POST['days_val']=="2"){ $s2=25; echo "$".$s2; } //day2
      	if($_POST['days_val']=="3"){ $s2=20; echo "$".$s2; } //day3
      	if($_POST['days_val']=="4"){ $s2=15; echo "$".$s2; } //day4
      	?>
      </td>
    </tr>
    <tr>
      <th >Sub Total</th>
      <!--Calculation of Items  -->
      <td >$<?php $subtotal=($_POST['product1']*10+$_POST['product2']*20+
      $_POST['product3']*30+@$s2); 
      echo $subtotal; ?></td> 
    </tr>
    <tr>
      <th >Taxes @13%</th>
      <td >$<?php $tax_value=$subtotal*(13/100); echo $tax_value; ?></td>  <!-- address of Tax -->
    </tr>
    <tr>
      <th >Total Billing</th>
      <td >$<?= $subtotal+$tax_value; ?></td>  <!--Total Value with Tax -->
    </tr>
</thead>
</table>
</div>
</div>
<h3><center>Thanks For Shopping</center> </h3>
<script type="text/javascript" src="files/index.js"></script>
</body>
</html>