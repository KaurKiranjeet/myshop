
### What is this repository for? ###

* This repo has source code for project named My-Shop. In this project user will able to create a cart. He has to provide his detail and
  quantity of products.
* After submit details, he will able to get invoice.


### How do I get set up? ###

* You need to XAMPP or if on windows platform you can use WAMP as well.
* Set the port of XAMPP/WAMP.

### How do I run project? ###

* Make sure your XAMP/WAMP is running successfully.
* Run index file in browser using (http://localhost:YOUR_PORT/FILE_PATH)

### Who do I talk? ###

* Repo owner is Kiranjeet Kaur. You can connect on kiranjeetk584@gmail.com

### Note :- ###
* License has been used with this repo. So, before use please read license file carefully.